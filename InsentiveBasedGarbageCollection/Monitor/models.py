from django.db import models
from datetime import datetime, timedelta

# Create your models here.

class RF_ID(models.Model):
    rf_id = models.CharField(max_length=20)
    user_id = models.IntegerField()
    balance = models.IntegerField(default=0)


class DustBin(models.Model):
    loc_id = models.CharField(max_length=20)
    last_clean = models.DateTimeField()
    weight = models.FloatField(default=0)
    status = models.CharField(max_length=10)
    loc_name = models.CharField(max_length=50)


class WeightPoints(models.Model):
    down_range = models.FloatField(default=0)
    up_range = models.FloatField(default=0)
    points = models.IntegerField(default=0)


class Coupons(models.Model):
    coupon_code = models.CharField(max_length=20)
    coupon_name = models.CharField(max_length=255)
    coupon_cost = models.IntegerField(default=0)
    image = models.CharField(max_length=255)


class UserCoupon(models.Model):
    user_id = models.IntegerField()
    coupon_id = models.IntegerField()
    coupon_status = models.BooleanField(default=True)
    coupon_expiry = models.DateTimeField(default=datetime.now()+timedelta(days=30))
    display_color = models.IntegerField(help_text='1=red,4=green')