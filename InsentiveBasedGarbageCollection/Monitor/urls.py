from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name= 'index'),
    path('account/login', views.login, name= 'login'),
    path('account/signup', views.signup, name= 'signup'),
    path('account/logout', views.logout, name= 'logout'),
    path('profile', views.profile, name= 'profile'),
    path('coupon', views.coupon, name= 'coupon'),
    path('coupon/getcoupon/<int:coupon_id>', views.getcoupon, name='getcoupon'),
    path('coupon/usecoupon/<int:usercoupon_id>', views.usecoupon, name='getcoupon'),
    path('contact', views.contact, name='contact'),
    path('dustbin', views.dustbin, name='dustbin')
]