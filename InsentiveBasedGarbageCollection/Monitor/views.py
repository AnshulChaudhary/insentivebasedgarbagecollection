from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.contrib import messages
from .models import Coupons, UserCoupon, DustBin, RF_ID

# Create your views here.

def index(request):

    return render(request, 'index.html')


def login(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            messages.info(request, 'Invalid username or password')
            return redirect('login')
    else:
        return render(request, 'account/login.html')


def signup(request):
    
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        
        if User.objects.filter(username=username).exists():
            messages.info(request, 'Username taken')
            return redirect('signup')
        elif User.objects.filter(email=email).exists():
            messages.info(request, 'email exists')
            return redirect('signup')
        elif password1 != password2:
            messages.info(request, 'password did not match')
            return redirect('signup')
        else:
            user = User.objects.create_user(username=username, email=email, password=password1)
            user.save()
            return redirect('login')
    else:    
        return render(request, 'account/signup.html')


def logout(request):

    auth.logout(request)
    return redirect('/')


def profile(request):

    if request.user.id:
        user = request.user
        rf_id = RF_ID.objects.get(user_id=user.id)
        usercoupons = UserCoupon.objects.filter(user_id=user.id)
        coupons = []
        for i in usercoupons:
            coupon = Coupons.objects.get(id=i.coupon_id)
            temp = [coupon,i]
            coupons.append(temp)
        return render(request, 'profile.html', {'rf_id':rf_id,'coupons':coupons})
    else:
        return redirect('/')

def coupon(request):

    if request.user.id:
        coupons = Coupons.objects.all()
        user = request.user
        rf_id = RF_ID.objects.get(user_id=user.id)
        return render(request, 'coupon.html', {'coupons':coupons,'rf_id':rf_id})
    else:
        coupons = Coupons.objects.all()
        return render(request, 'coupon.html', {'coupons':coupons})

def getcoupon(request, coupon_id):

    if request.user.id:
        user = request.user
        coupon = Coupons.objects.get(id=coupon_id)
        rf_id = RF_ID.objects.get(user_id=user.id)
        user_id = request.user.id
        coupon_status = 1
        coupon_color = 4
        if rf_id.balance >= coupon.coupon_cost:
            new_balance = rf_id.balance-coupon.coupon_cost
            rf_id.balance = new_balance
            rf_id.save()
            usercoupon = UserCoupon.objects.create(user_id=user_id, coupon_id=coupon_id, coupon_status=coupon_status, display_color=coupon_color)
            usercoupon.save()
        else:
            messages.info(request, 'Not enough Token to Get the Coupon Code')
        return redirect('/coupon')

    else:
        return redirect('/')

def usecoupon(request, usercoupon_id):
    
    if request.user.id:
        usercoupon = UserCoupon.objects.get(id=usercoupon_id)
        usercoupon.coupon_status = 0
        usercoupon.display_color = 1
        usercoupon.save()
        messages.info(request, 'Coupon code Used, Thank You!')
        return redirect('/profile')
    else:
        return redirect('/')

def contact(request):
    return render(request, 'contact.html')

def dustbin(request):
    
    if request.user.id and request.user.is_superuser:
        dustbins = DustBin.objects.all()
        return render(request, 'dustbin.html', {'dustbins':dustbins})
