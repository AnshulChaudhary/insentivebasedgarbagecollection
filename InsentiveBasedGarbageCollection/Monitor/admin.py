from django.contrib import admin
from .models import RF_ID, DustBin, WeightPoints, Coupons, UserCoupon

# Register your models here.

admin.site.register(RF_ID)
admin.site.register(DustBin)
admin.site.register(WeightPoints)
admin.site.register(Coupons)
admin.site.register(UserCoupon)
